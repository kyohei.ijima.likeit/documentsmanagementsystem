package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Car;
import beans.Customer;
import beans.Staff;
import dao.CarDao;
import dao.CustomerDao;

/**
 * Servlet implementation class CuctomerSearch
 */
@WebServlet("/CustomerSearch")
public class CustomerSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerSearch() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		//車種情報取得
		CarDao carDao = new CarDao();
		List<Car> carList = carDao.findAll();
		// リクエストスコープに車種情報をセット
		request.setAttribute("carList", carList);

		//スタッフID取得
		String loginId = logSes.getLoginId();
		//顧客情報取得
		CustomerDao customerDao = new CustomerDao();
		List<Customer> customerList = customerDao.findStaffsCustomer(loginId);
		// リクエストスコープに顧客情報をセット
		request.setAttribute("customerList", customerList);

		// 顧客一覧画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerSearch.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//form情報取得
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String age = request.getParameter("age");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");
		String receivedOrderId = request.getParameter("receivedOrderId");
		String car = request.getParameter("car");
		String buyMethod = request.getParameter("buyMethod");
		String tradeInCar = request.getParameter("tradeInCar");
		String insurance = request.getParameter("insurance");

		//車種情報取得
		CarDao carDao = new CarDao();
		List<Car> carList = carDao.findAll();
		// リクエストスコープに車種情報をセット
		request.setAttribute("carList", carList);

		//スタッフID取得
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		String loginId = logSes.getLoginId();

		//検索処理
		CustomerDao customerDao = new CustomerDao();
		List<Customer> customerList = customerDao.customerSeach(name, address, age, birthDate, id, receivedOrderId, car,
				buyMethod, tradeInCar, insurance, loginId);
		//リクエスト
		request.setAttribute("customerList", customerList);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerSearch.jsp");
		dispatcher.forward(request, response);

	}

}
