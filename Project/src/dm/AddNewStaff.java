package dm;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Staff;
import dao.StaffDao;

/**
 * Servlet implementation class AddNewStaff
 */
@WebServlet("/AddNewStaff")
public class AddNewStaff extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewStaff() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");

		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		// スタッフ追加画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addNewStaff.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//既に登録されているログインIDが入力された場合
		String loginId = request.getParameter("loginId");

		StaffDao staffDao = new StaffDao();
		boolean a = staffDao.findSameLoginId(loginId);

		if (a == false) {
			request.setAttribute("errMsgLog", "すでに登録されているログインIDがあります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addNewStaff.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードとパスワード（確認）の入力が異なる場合
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");

		if (!password.equals(password1)) {
			request.setAttribute("errMsgPass", "パスワードとパスワード（確認）の入力が異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addNewStaff.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//未入力項目がある場合
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		boolean isCheckOK = true;
		String[] signUpForm = { loginId, password, password1, name, birthDate };
		for (int i = 0; i < signUpForm.length; i++) {
			if (signUpForm[i].equals("")) {
				isCheckOK = false;
			}
		}

		if(!isCheckOK) {
			request.setAttribute("errMsgNull", "未入力項目があります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addNewStaff.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//スタッフ新規登録
		staffDao.InsertStaff(loginId, password, name, birthDate);
		//スタッフ一覧画面へフォワード
		response.sendRedirect("StaffList");
	}

}
