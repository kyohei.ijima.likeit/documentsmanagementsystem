package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Customer;
import beans.Documents;
import beans.DocumentsManagement;
import dao.CustomerDao;
import dao.DocumentsDao;
import dao.DocumentsManagementDao;

/**
 * Servlet implementation class CustomerDetail
 */
@WebServlet("/CustomerDetail")
public class CustomerDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("customerId");
		//idを引数にして、idに紐づく顧客情報を出力する
		CustomerDao customerDao = new CustomerDao();
		Customer customer = customerDao.CustomerDetail(id);
		//顧客情報をリクエストスコープにセット
		request.setAttribute("customer", customer);
		//顧客の書類情報取得
		DocumentsManagementDao dm = new DocumentsManagementDao();
		List<DocumentsManagement> dmList = dm.getSubmissionStatus(id);
		request.setAttribute("dmList", dmList);
//		//書類情報取得
//		DocumentsDao documentsDao = new DocumentsDao();
//		List<Documents> documentsList = documentsDao.necessaryDocuments(id);
//		//購入方法
//		List<Documents> buyMethodDocumentsList = documentsDao.necessaryDocumentsByBuyMethod(id, customer.getBuyMethod());
//		if(buyMethodDocumentsList != null) {
//			for(Documents documents : buyMethodDocumentsList)
//			documentsList.add(documents);
//		}
//		//下取り有無
//		List<Documents> tradeInCarDocumentsList = documentsDao.necessaryDocumentsByTradeInCar(id, customer.getTradeInCar());
//		if(tradeInCarDocumentsList != null) {
//			for(Documents documents : tradeInCarDocumentsList)
//			documentsList.add(documents);
//		}
//		//保険
//		List<Documents> insuranceDocumentsList = documentsDao.necessaryDocumentsByInsurance(id, customer.getInsurance());
//		if(insuranceDocumentsList != null) {
//			for(Documents documents : insuranceDocumentsList)
//			documentsList.add(documents);
//		}
//		//書類情報をリクエストスコープにセット
//		request.setAttribute("documentsList", documentsList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerDetail.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// GETパラメータとしてフォーム情報を受け取る
		String id = request.getParameter("customerId");
		String[] documentId = request.getParameterValues("documentId");

		//顧客情報取得
		CustomerDao customerDao = new CustomerDao();
		Customer customer = customerDao.CustomerDetail(id);
		//顧客情報をリクエストスコープにセット
		request.setAttribute("customer", customer);

		//書類提出状況更新
		DocumentsManagementDao dm = new DocumentsManagementDao();
		for(String documentId1 : documentId) {
			dm.submissionDocuments(documentId1, id);
		}
		//顧客の書類情報取得
		List<DocumentsManagement> dmList = dm.getSubmissionStatus(id);
		//提出状況確認
		boolean isCheckOK = true;
		for(DocumentsManagement submissionCheck : dmList ) {
			if (submissionCheck.getSubmissionStatus() == 0) {
				isCheckOK = false;
			}
		}
		//提出状況に応じて処理の分岐
		if(!isCheckOK) {
			//書類情報をリクエストスコープにセット
			request.setAttribute("dmList", dmList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerDetail.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(isCheckOK) {
			//ステータス更新
			int newStatusId = customer.getStatusId() + 1;
			customerDao.statusUpdate(Integer.toString(newStatusId), id);
//			return;
		}

		//ステータス更新後の必要書類取得
		DocumentsDao documentsDao = new DocumentsDao();
		List<Documents> documentsList = documentsDao.necessaryDocuments(id);
		//購入方法による書類
		List<Documents> buyMethodDocumentsList = documentsDao.necessaryDocumentsByBuyMethod(id, customer.getBuyMethod());
		if(buyMethodDocumentsList != null) {
			for(Documents documents : buyMethodDocumentsList)
			documentsList.add(documents);
		}
		//下取り有無による書類
		List<Documents> tradeInCarDocumentsList = documentsDao.necessaryDocumentsByTradeInCar(id, customer.getTradeInCar());
		if(tradeInCarDocumentsList != null) {
			for(Documents documents : tradeInCarDocumentsList)
			documentsList.add(documents);
		}
		//保険有無による書類
		List<Documents> insuranceDocumentsList = documentsDao.necessaryDocumentsByInsurance(id, customer.getInsurance());
		if(insuranceDocumentsList != null) {
			for(Documents documents : insuranceDocumentsList)
			documentsList.add(documents);
		}
		//必要書類登録
		for(Documents documentId1 : documentsList) {
			dm.insertNewDocuments(Integer.toString(documentId1.getId()), id);
		}
		//顧客の書類情報取得
		List<DocumentsManagement> newDmList = dm.getSubmissionStatus(id);


		//書類情報をリクエストスコープにセット
		request.setAttribute("dmList", newDmList);
		//新たに顧客情報取得
		Customer customer1 = customerDao.CustomerDetail(id);
		//顧客情報をリクエストスコープにセット
		request.setAttribute("customer", customer1);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerDetail.jsp");
		dispatcher.forward(request, response);


//		//書類提出状況確認
//		List<DocumentsManagement> dmStatusList = dm.getSubmissionStatus(id);
//		request.setAttribute("dmStatusList", dmStatusList);
//
//		//書類情報取得
//		DocumentsDao documentsDao = new DocumentsDao();
//		List<Documents> documentsList = documentsDao.necessaryDocuments(id);
//		//購入方法による書類
//		List<Documents> buyMethodDocumentsList = documentsDao.necessaryDocumentsByBuyMethod(id, customer.getBuyMethod());
//		if(buyMethodDocumentsList != null) {
//			for(Documents documents : buyMethodDocumentsList)
//			documentsList.add(documents);
//		}
//		//下取り有無による書類
//		List<Documents> tradeInCarDocumentsList = documentsDao.necessaryDocumentsByTradeInCar(id, customer.getTradeInCar());
//		if(tradeInCarDocumentsList != null) {
//			for(Documents documents : tradeInCarDocumentsList)
//			documentsList.add(documents);
//		}
//		//保険有無による書類
//		List<Documents> insuranceDocumentsList = documentsDao.necessaryDocumentsByInsurance(id, customer.getInsurance());
//		if(insuranceDocumentsList != null) {
//			for(Documents documents : insuranceDocumentsList)
//			documentsList.add(documents);
//		}
//		//書類情報をリクエストスコープにセット
//		request.setAttribute("documentsList", documentsList);

//		//フォワード
//		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerDetail.jsp");
//		dispatcher.forward(request, response);

	}

}
