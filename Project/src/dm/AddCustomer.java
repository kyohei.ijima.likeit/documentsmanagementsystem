package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Car;
import beans.Customer;
import beans.Documents;
import beans.Staff;
import dao.CarDao;
import dao.CustomerDao;
import dao.DocumentsDao;
import dao.DocumentsManagementDao;
import dao.StaffDao;

/**
 * Servlet implementation class AddCustomer
 */
@WebServlet("/AddCustomer")
public class AddCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddCustomer() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		//車種情報取得
		CarDao carDao = new CarDao();
		List<Car> carList = carDao.findAll();
		// リクエストスコープに車種情報をセット
		request.setAttribute("carList", carList);

		// Top画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addCustomer.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//form情報取得
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String age = request.getParameter("age");
		String birthDate = request.getParameter("birthDate");
		String car = request.getParameter("car");
		String buyMethod = request.getParameter("buyMethod");
		String tradeInCar = request.getParameter("tradeInCar");
		String insurance = request.getParameter("insurance");

		//未入力項目がある場合;
		boolean isCheckOK = true;
		String[] signUpForm = {name, birthDate, age, birthDate, car, buyMethod, tradeInCar, insurance};
		for (int i = 0; i < signUpForm.length; i++) {
			if (signUpForm[i].equals("")) {
				isCheckOK = false;
			}
		}
		if(!isCheckOK) {
			//車種情報取得
			CarDao carDao = new CarDao();
			List<Car> carList = carDao.findAll();
			// リクエストスコープに車種情報をセット
			request.setAttribute("carList", carList);
			request.setAttribute("errMsgNull", "未入力項目があります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addCustomer.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//スタッフID取得
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		int id = logSes.getId();

		//スタッフの最大受注番号検索、作成
		StaffDao staffDao = new StaffDao();
		int maxReceivedOrderId = staffDao.MaxReceivedOrderId(id);

		//登録処理
		CustomerDao customerDao = new CustomerDao();
		int newCustomerId = customerDao.addCustomer(name, address, age, birthDate, car, buyMethod, tradeInCar, insurance, id,
				maxReceivedOrderId);
		//新規顧客情報取得
		String newCustomerId1 = String.valueOf(newCustomerId);
		Customer newCustomer = customerDao.CustomerDetail(newCustomerId1);
		request.setAttribute("newCustomer", newCustomer);

		//受注時必要書類
		DocumentsDao documentsDao = new DocumentsDao();
		List<Documents> documentsList = documentsDao.necessaryDocuments(newCustomerId1);
		//購入方法による必要書類
		List<Documents> buyMethodDocumentsList = documentsDao.necessaryDocumentsByBuyMethod(newCustomerId1, newCustomer.getBuyMethod());
		if(buyMethodDocumentsList != null) {
			for(Documents documents : buyMethodDocumentsList)
			documentsList.add(documents);
		}
		request.setAttribute("documentsList", documentsList);
		//必要書類登録
		DocumentsManagementDao dm = new DocumentsManagementDao();
		for(Documents documentId1 : documentsList) {
			dm.insertNewDocuments(Integer.toString(documentId1.getId()), newCustomerId1);
		}

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/orderDocuments.jsp");
		dispatcher.forward(request, response);

	}
}
