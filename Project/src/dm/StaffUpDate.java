package dm;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Staff;
import dao.StaffDao;

/**
 * Servlet implementation class StaffUpDate
 */
@WebServlet("/StaffUpDate")
public class StaffUpDate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaffUpDate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");

		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		// idを引数にして、idに紐づくユーザ情報を出力する
		StaffDao staffDao = new StaffDao();
		Staff staff = staffDao.staffDetail(id);

		// ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("staffDetail", staff);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staffUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//入力値取得
		int id = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		//パスワードとパスワード（確認）の入力が異なる場合
		if (!password.equals(password1)) {
			request.setAttribute("errMsgPass", "パスワードとパスワード（確認）の入力が異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staffUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//未入力項目がある場合
		boolean isCheckOK = true;
		String[] signUpForm = { name, birthDate };
		for (int i = 0; i < signUpForm.length; i++) {
			if (signUpForm[i].equals("")) {
				isCheckOK = false;
			}
		}
		if (!isCheckOK) {
			request.setAttribute("errMsgNull", "未入力項目があります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staffUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//更新
		StaffDao staffDao = new StaffDao();
		if (password.equals("")) {
			staffDao.UpdateStaff(id, name, birthDate);
			response.sendRedirect("StaffList");
			return;
		}else if(!password.equals("")) {
			staffDao.UpdateStaff(id, password, name, birthDate);
			response.sendRedirect("StaffList");
			return;
		}
	}

}
