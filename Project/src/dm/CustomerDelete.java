package dm;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import beans.Staff;
import dao.CustomerDao;

/**
 * Servlet implementation class CustomerDelete
 */
@WebServlet("/CustomerDelete")
public class CustomerDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		// idを引数にして、idに紐づくユーザ情報を出力する
		CustomerDao customerDao = new CustomerDao();
		Customer customer = customerDao.CustomerDetail(id);

		// 顧客情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("customer", customer);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		CustomerDao customerDao = new CustomerDao();
		customerDao.customerDelete(id);

		response.sendRedirect("AllCustomerSearch");
	}

}
