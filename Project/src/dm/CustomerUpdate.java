package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Car;
import beans.Customer;
import beans.Staff;
import dao.CarDao;
import dao.CustomerDao;

/**
 * Servlet implementation class CustomerUpdate
 */
@WebServlet("/CustomerUpdate")
public class CustomerUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		//車種情報取得
		CarDao carDao = new CarDao();
		List<Car> carList = carDao.findAll();
		// リクエストスコープに車種情報をセット
		request.setAttribute("carList", carList);

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		//idから顧客情報取得
		CustomerDao customerDao = new CustomerDao();
		Customer customer = customerDao.CustomerDetail(id);
		// リクエストスコープに顧客情報をセット
		request.setAttribute("customer", customer);

		// 顧客更新画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customerUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//form情報取得
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String age = request.getParameter("age");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");
		String receivedOrderId = request.getParameter("receivedOrderId");
//		String car = request.getParameter("car");
		String buyMethod = request.getParameter("buyMethod");
		String tradeInCar = request.getParameter("tradeInCar");
		String insurance = request.getParameter("insurance");

		//顧客情報更新
		CustomerDao customerDao = new CustomerDao();
		customerDao.customerUpdate(name, address, age, birthDate, id, receivedOrderId, buyMethod, tradeInCar,
				insurance);
		//全顧客画面へリダイレクト
		response.sendRedirect("AllCustomerSearch");

	}

}
