package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Car;
import beans.Customer;
import beans.Staff;
import dao.CarDao;
import dao.CustomerDao;

/**
 * Servlet implementation class AllCustomerSearch
 */
@WebServlet("/AllCustomerSearch")
public class AllCustomerSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllCustomerSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		//車種情報取得
		CarDao carDao = new CarDao();
		List<Car> carList = carDao.findAll();
		// リクエストスコープに車種情報をセット
		request.setAttribute("carList", carList);

		//顧客情報取得
		CustomerDao customerDao = new CustomerDao();
		List<Customer> customerList = customerDao.findAll();
		// リクエストスコープに顧客情報をセット
		request.setAttribute("customerList", customerList);

		// 全顧客画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/allCustomerSearch.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
