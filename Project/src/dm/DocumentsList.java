package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Documents;
import beans.Staff;
import dao.DocumentsDao;

/**
 * Servlet implementation class DocumentsList
 */
@WebServlet("/DocumentsList")
public class DocumentsList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DocumentsList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		//書類情報取得
		DocumentsDao documentsDao = new DocumentsDao();
		List<Documents> documentsList1 = documentsDao.findByStatusId(1);
		List<Documents> documentsList2 = documentsDao.findByStatusId(2);
		List<Documents> documentsList3 = documentsDao.findByStatusId(3);
		List<Documents> documentsList4 = documentsDao.findByStatusId(4);

		//スコープ
		request.setAttribute("documentsList1", documentsList1);
		request.setAttribute("documentsList2", documentsList2);
		request.setAttribute("documentsList3", documentsList3);
		request.setAttribute("documentsList4", documentsList4);

		// 書類一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/documentsList.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
