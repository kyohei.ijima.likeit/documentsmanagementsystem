package dm;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Staff;
import dao.StaffDao;

/**
 * Servlet implementation class StaffList
 */
@WebServlet("/StaffList")
public class StaffList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaffList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");

		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		// ユーザ一覧情報を取得
		StaffDao staffDao = new StaffDao();
		List<Staff> staffList = staffDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("staffList", staffList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staffList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//form情報取得
		String loginId = request.getParameter("login-id");
		String name = request.getParameter("name");
		String dateSt = request.getParameter("date-start");
		String dateEd = request.getParameter("date-end");

		StaffDao staffDao = new StaffDao();
		List<Staff> staffList = staffDao.searchStaff(loginId , name , dateSt , dateEd);


		request.setAttribute("staffList", staffList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staffList.jsp");
		dispatcher.forward(request, response);
	}

}
