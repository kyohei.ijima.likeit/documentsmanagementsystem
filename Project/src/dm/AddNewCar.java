package dm;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Staff;
import dao.CarDao;

/**
 * Servlet implementation class AddNewCar
 */
@WebServlet("/AddNewCar")
public class AddNewCar extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewCar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");

		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		// 車種追加画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addNewCar.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//未入力項目がある場合
		String name = request.getParameter("name");
		String type = request.getParameter("type");

		boolean isCheckOK = true;
		String[] signUpForm = {name, type};
		for (int i = 0; i < signUpForm.length; i++) {
			if (signUpForm[i].equals("")) {
				isCheckOK = false;
			}
		}

		if(!isCheckOK) {
			request.setAttribute("errMsgNull", "未入力項目があります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addNewStaff.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//車種新規登録
		CarDao carDao = new CarDao();
		carDao.InsertCar(name, type);
		//スタッフ一覧画面へフォワード
		response.sendRedirect("CarList");
	}

}
