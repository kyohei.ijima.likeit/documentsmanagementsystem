package dm;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Documents;
import beans.Staff;
import dao.DocumentsDao;

/**
 * Servlet implementation class DocumentDetail
 */
@WebServlet("/DocumentDetail")
public class DocumentDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DocumentDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報確認
		HttpSession session = request.getSession();
		Staff logSes = (Staff) session.getAttribute("staffInfo");
		if (logSes == null) {
			response.sendRedirect("Login");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		//idから書類詳細情報取得
		DocumentsDao documentsDao = new DocumentsDao();
		Documents document = documentsDao.findById(id);
		//書類詳細情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("document", document);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/documentDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
