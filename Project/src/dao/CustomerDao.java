package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Customer;

public class CustomerDao {

	public List<Customer> findAll() {
		Connection conn = null;
		List<Customer> customerList = new ArrayList<Customer>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM customer";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				int receivedOrderId = rs.getInt("received_order_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				int age = rs.getInt("age");
				Date birthDate = rs.getDate("birth_date");
				int carId = rs.getInt("car_id");
				int buyMethod = rs.getInt("buy_method");
				int tradeInCar = rs.getInt("trade_in_car");
				int insurance = rs.getInt("insurance");
				int staffId = rs.getInt("staff_id");
				int statusId = rs.getInt("status_id");
				Customer customer = new Customer(id, receivedOrderId, name, address, age, birthDate, carId, buyMethod,
						tradeInCar, insurance, staffId, statusId);

				customerList.add(customer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return customerList;
	}

	public List<Customer> findStaffsCustomer(String loginId) {
		Connection conn = null;
		List<Customer> customerList = new ArrayList<Customer>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM customer " +
					"JOIN staff " +
					"ON customer.staff_id = staff.id " +
					"WHERE staff.login_id = ? " +
					"AND status_id != 5";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				int receivedOrderId = rs.getInt("received_order_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				int age = rs.getInt("age");
				Date birthDate = rs.getDate("birth_date");
				int carId = rs.getInt("car_id");
				int buyMethod = rs.getInt("buy_method");
				int tradeInCar = rs.getInt("trade_in_car");
				int insurance = rs.getInt("insurance");
				int staffId = rs.getInt("staff_id");
				int statusId = rs.getInt("status_id");
				Customer customer = new Customer(id1, receivedOrderId, name, address, age, birthDate, carId, buyMethod,
						tradeInCar, insurance, staffId, statusId);

				customerList.add(customer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return customerList;
	}

	public List<Customer> finishCustomerList(String loginId) {
		Connection conn = null;
		List<Customer> customerList = new ArrayList<Customer>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM customer " +
					"JOIN staff " +
					"ON customer.staff_id = staff.id " +
					"WHERE staff.login_id = ? " +
					"AND status_id = 5";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				int receivedOrderId = rs.getInt("received_order_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				int age = rs.getInt("age");
				Date birthDate = rs.getDate("birth_date");
				int carId = rs.getInt("car_id");
				int buyMethod = rs.getInt("buy_method");
				int tradeInCar = rs.getInt("trade_in_car");
				int insurance = rs.getInt("insurance");
				int staffId = rs.getInt("staff_id");
				int statusId = rs.getInt("status_id");
				Customer customer = new Customer(id1, receivedOrderId, name, address, age, birthDate, carId, buyMethod,
						tradeInCar, insurance, staffId, statusId);

				customerList.add(customer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return customerList;
	}

	public Customer CustomerDetail(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM customer WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id1 = rs.getInt("id");
			int receivedOrderId = rs.getInt("received_order_id");
			String name = rs.getString("name");
			String address = rs.getString("address");
			int age = rs.getInt("age");
			Date birthDate = rs.getDate("birth_date");
			int carId = rs.getInt("car_id");
			int buyMethod = rs.getInt("buy_method");
			int tradeInCar = rs.getInt("trade_in_car");
			int insurance = rs.getInt("insurance");
			int staffId = rs.getInt("staff_id");
			int statusId = rs.getInt("status_id");
			Customer customer = new Customer(id1, receivedOrderId, name, address, age, birthDate, carId, buyMethod,
					tradeInCar, insurance, staffId, statusId);
			return customer;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<Customer> customerSeach(String nameP, String addressP, String ageP, String birthDateP, String idP,
			String receivedOrderIdP, String carP, String buyMethodP, String tradeInCarP, String insuranceP,
			String loginId) {

		Connection conn = null;
		List<Customer> customerList = new ArrayList<Customer>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM customer " +
					"JOIN staff " +
					"ON customer.staff_id = staff.id " +
					"WHERE staff.login_id = ? ";

			if (!nameP.equals("")) {
				sql += " AND name LIKE '" + '%' + nameP + '%' + "' ";
			}
			if (!addressP.equals("")) {
				sql += " AND address LIKE '" + '%' + addressP + '%' + "' ";
			}
			if (!ageP.equals("")) {
				sql += " AND age LIKE '" + ageP + "' ";
			}
			if (!birthDateP.equals("")) {
				sql += " AND birth_date LIKE '" + birthDateP + "' ";
			}
			if (!idP.equals("")) {
				sql += " AND id = '" + idP + "' ";
			}
			if (!receivedOrderIdP.equals("")) {
				sql += " AND received_order_id = '" + receivedOrderIdP + "' ";
			}
			if (!carP.equals("")) {
				sql += " AND car_id = '" + carP + "' ";
			}
			if (buyMethodP != null) {
				sql += " AND buy_method = '" + buyMethodP + "' ";
			}
			if (tradeInCarP != null) {
				sql += " AND trade_in_car = '" + tradeInCarP + "' ";
			}
			if (insuranceP != null) {
				sql += " AND insurance = '" + insuranceP + "' ";
			}

			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				int receivedOrderId = rs.getInt("received_order_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				int age = rs.getInt("age");
				Date birthDate = rs.getDate("birth_date");
				int carId = rs.getInt("car_id");
				int buyMethod = rs.getInt("buy_method");
				int tradeInCar = rs.getInt("trade_in_car");
				int insurance = rs.getInt("insurance");
				int staffId = rs.getInt("staff_id");
				int statusId = rs.getInt("status_id");
				Customer customer = new Customer(id1, receivedOrderId, name, address, age, birthDate, carId, buyMethod,
						tradeInCar, insurance, staffId, statusId);

				customerList.add(customer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return customerList;
	}

	public List<Customer> finishedCustomerSeach(String nameP, String addressP, String ageP, String birthDateP,
			String idP,
			String receivedOrderIdP, String carP, String buyMethodP, String tradeInCarP, String insuranceP,
			String loginId) {

		Connection conn = null;
		List<Customer> customerList = new ArrayList<Customer>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM customer " +
					"JOIN staff " +
					"ON customer.staff_id = staff.id " +
					"WHERE staff.login_id = ? " +
					"AND  status_id = 5";

			if (!nameP.equals("")) {
				sql += " AND name LIKE '" + '%' + nameP + '%' + "' ";
			}
			if (!addressP.equals("")) {
				sql += " AND address LIKE '" + '%' + addressP + '%' + "' ";
			}
			if (!ageP.equals("")) {
				sql += " AND age LIKE '" + ageP + "' ";
			}
			if (!birthDateP.equals("")) {
				sql += " AND birth_date LIKE '" + birthDateP + "' ";
			}
			if (!idP.equals("")) {
				sql += " AND id = '" + idP + "' ";
			}
			if (!receivedOrderIdP.equals("")) {
				sql += " AND received_order_id = '" + receivedOrderIdP + "' ";
			}
			if (!carP.equals("")) {
				sql += " AND car_id = '" + carP + "' ";
			}
			if (buyMethodP != null) {
				sql += " AND buy_method = '" + buyMethodP + "' ";
			}
			if (tradeInCarP != null) {
				sql += " AND trade_in_car = '" + tradeInCarP + "' ";
			}
			if (insuranceP != null) {
				sql += " AND insurance = '" + insuranceP + "' ";
			}

			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				int receivedOrderId = rs.getInt("received_order_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				int age = rs.getInt("age");
				Date birthDate = rs.getDate("birth_date");
				int carId = rs.getInt("car_id");
				int buyMethod = rs.getInt("buy_method");
				int tradeInCar = rs.getInt("trade_in_car");
				int insurance = rs.getInt("insurance");
				int staffId = rs.getInt("staff_id");
				int statusId = rs.getInt("status_id");
				Customer customer = new Customer(id1, receivedOrderId, name, address, age, birthDate, carId, buyMethod,
						tradeInCar, insurance, staffId, statusId);

				customerList.add(customer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return customerList;
	}

	public int addCustomer(String name, String address, String age, String birthDate,
			String car, String buyMethod, String tradeInCar, String insurance, int id, int maxReceivedOrderId) {
		int autoIncKey = -1;
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "INSERT INTO customer(received_order_id, name, address, age, birth_date, car_id, buy_method, trade_in_car, insurance, staff_id, status_id) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pStmt.setInt(1, maxReceivedOrderId);
			pStmt.setString(2, name);
			pStmt.setString(3, address);
			pStmt.setString(4, age);
			pStmt.setString(5, birthDate);
			pStmt.setString(6, car);
			pStmt.setString(7, buyMethod);
			pStmt.setString(8, tradeInCar);
			pStmt.setString(9, insurance);
			pStmt.setInt(10, id);

			pStmt.executeUpdate();

			ResultSet rs = pStmt.getGeneratedKeys();

			if (rs.next()) { //顧客idのみ戻り値にする場合
				autoIncKey = rs.getInt(1);
			}
			return autoIncKey;

			//			if (!rs.next()) {
			//				return null;
			//			}
			//			int id1 = rs.getInt("id");
			//			int receivedOrderId = rs.getInt("received_order_id");
			//			String name1 = rs.getString("name");
			//			String address1 = rs.getString("address");
			//			int age1 = rs.getInt("age");
			//			Date birthDate1 = rs.getDate("birth_date");
			//			int carId = rs.getInt("car_id");
			//			int buyMethod1 = rs.getInt("buy_method");
			//			int tradeInCar1 = rs.getInt("trade_in_car");
			//			int insurance1 = rs.getInt("insurance");
			//			int staffId = rs.getInt("staff_id");
			//			int statusId = rs.getInt("status_id");
			//			Customer customer = new Customer(id1, receivedOrderId, name1, address1, age1, birthDate1, carId, buyMethod1,
			//					tradeInCar1, insurance1, staffId, statusId);
			//			return customer;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
					return autoIncKey;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return autoIncKey;
	}

	public void customerUpdate(String name, String address, String age, String birthDate, String id,
			String receivedOrderId, String buyMethod, String tradeInCar, String insurance) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE customer SET name = ?, address = ?, age = ?, birth_date = ?, received_order_id = ?, buy_method = ?, trade_in_car = ?, insurance = ? WHERE id = ?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, address);
			pStmt.setString(3, age);
			pStmt.setString(4, birthDate);
			pStmt.setString(5, receivedOrderId);
//			pStmt.setString(6, car);
			pStmt.setString(6, buyMethod);
			pStmt.setString(7, tradeInCar);
			pStmt.setString(8, insurance);
			pStmt.setString(9, id);


			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void customerDelete(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "DELETE FROM customer WHERE id = ?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void statusUpdate(String statusId, String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "UPDATE customer "
					+ "SET status_id = ?"
					+ "WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, statusId);
			pStmt.setString(2, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
