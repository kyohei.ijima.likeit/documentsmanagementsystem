package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.Staff;

public class StaffDao {

	public Staff findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM staff WHERE login_id = ? and password = ?";

			//password暗号化
			String result = cypher(password);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			int id = rs.getInt("id");
			String loginId1 = rs.getString("login_id");
			String name = rs.getString("staff_name");
			Date birthDate = rs.getDate("birth_date");
			String password1 = rs.getString("password");
			String createDate = rs.getString("birth_date");
			String updateDate = rs.getString("birth_date");
			return new Staff(id, loginId1, name, birthDate, password1, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public Staff findById(int id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM staff WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("staff_name");
			return new Staff(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public int MaxReceivedOrderId(int id) {

		Connection conn = null;
		int a = -1;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT MAX(received_order_id)" +
					" FROM customer" +
					" WHERE staff_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return a;
			}

			int maxReceivedOrderId = rs.getInt("MAX(received_order_id)");
			return maxReceivedOrderId + 1;

		} catch (SQLException e) {
			e.printStackTrace();
			return a;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return a;
				}
			}
		}
	}

	public List<Staff> findAll() {
		Connection conn = null;
		List<Staff> staffList = new ArrayList<Staff>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM staff";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("staff_name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Staff staff = new Staff(id, loginId, name, birthDate, password, createDate, updateDate);

				staffList.add(staff);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return staffList;
	}

	public Staff staffDetail(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM staff WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("staff_name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			Staff staff = new Staff(id1, loginId, name, birthDate, password, createDate, updateDate);
			return staff;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UpdateStaff(int id , String password, String name, String birthDate) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE staff SET password = ? , staff_name = ? , birth_date = ? , update_date = NOW() WHERE id = ?;";

			//password暗号化
			String result = cypher(password);
			password = result;

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setInt(4, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void UpdateStaff(int id , String name, String birthDate) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE staff SET staff_name = ? , birth_date = ? , update_date = NOW() WHERE id = ?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setInt(3, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void staffDelete(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "DELETE FROM staff WHERE id = ?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean findSameLoginId(String loginId) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "SERECT login_id FROM staff WHERE login_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				return false;
			}
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public void InsertStaff(String loginId, String password, String name, String birthDate){

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "INSERT INTO staff(login_id , password , staff_name , birth_date , create_date , update_date) VALUES (? , ? , ? , ? , NOW() , NOW())";

			//password暗号化
			String result = cypher(password);
			password = result;

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Staff> searchStaff(String loginIdP, String nameP , String dateStP , String dateEdP) {
		Connection conn = null;
		List<Staff> staffList = new ArrayList<Staff>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM staff";

			if(!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "' ";
			}
			if(!nameP.equals("")) {
				sql += " AND name LIKE '" + '%' + nameP + '%' + "' ";
			}
			if(!dateStP.equals("")) {
				sql += " AND birth_date >= '" + dateStP + "' ";
			}
			if(!dateEdP.equals("")) {
				sql += " AND birth_date <= '" + dateEdP + "' ";
			}

			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("staff_name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Staff staff = new Staff(id, loginId, name, birthDate, password, createDate, updateDate);

				staffList.add(staff);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return staffList;
	}

	public String cypher(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}

}
