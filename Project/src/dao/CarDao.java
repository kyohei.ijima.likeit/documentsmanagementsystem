package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Car;

public class CarDao {

	public List<Car> findAll() {
		Connection conn = null;
		List<Car> carList = new ArrayList<Car>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM car";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("car_name");
				String type = rs.getString("type");
				Car car = new Car(id, name, type);

				carList.add(car);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return carList;
	}

	public Car findById(int id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM car WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			int idData = rs.getInt("id");
			String nameData = rs.getString("car_name");
			String typeData = rs.getString("type");
			return new Car(idData, nameData, typeData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void CarDelete(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "DELETE FROM car WHERE id = ?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void InsertCar(String name, String type) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "INSERT INTO car(car_name , type) VALUES (?, ?)";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, type);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
