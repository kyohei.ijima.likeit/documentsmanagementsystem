package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.DocumentsManagement;

public class DocumentsManagementDao {

	public void insertNewDocuments(String documentId, String id){

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "INSERT INTO documents_management(documents_id, submission_status, customer_id) VALUES (? , 0 , ?)";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, documentId);
			pStmt.setString(2, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void submissionDocuments(String documentId, String id){

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE documents_management "
					+ "SET submission_status = 1 "
					+ "WHERE documents_id = ? "
					+ "AND customer_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, documentId);
			pStmt.setString(2, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<DocumentsManagement> getSubmissionStatus(String customerId) {
		Connection conn = null;
		List<DocumentsManagement> dmList = new ArrayList<DocumentsManagement>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * "
					+ "FROM documents_management "
					+ "INNER JOIN customer "
					+ "ON customer.id = documents_management.customer_id "
					+ "INNER JOIN documents "
					+ "ON documents.id = documents_management.documents_id "
					+ "WHERE customer.id = ? "
					+ "AND customer.status_id = documents.status_id";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, customerId);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				int documentsId = rs.getInt("documents_id");
				int submissionStatus = rs.getInt("submission_status");
				int customerId1 = rs.getInt("customer_id");
				DocumentsManagement dm = new DocumentsManagement(id, documentsId, submissionStatus, customerId1);

				dmList.add(dm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return dmList;
	}

}
