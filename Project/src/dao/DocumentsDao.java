package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Documents;

public class DocumentsDao {

	public List<Documents> findAll() {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM documents";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> findByStatusId(int statusIdP) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM documents WHERE status_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, statusIdP);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public Documents findById(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM documents WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			int id1 = rs.getInt("id");
			String name = rs.getString("documents_name");
			String file = rs.getString("file");
			int statusId = rs.getInt("status_id");
			return new Documents(id1, name, file, statusId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<Documents> necessaryDocuments(String id) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM documents " +
					"JOIN customer " +
					"ON documents.status_id = customer.status_id " +
					"WHERE customer.id = ? " +
					"AND documents.buy_method_id = 0 " +
					"AND documents.trade_in_car_id = 0 " +
					"AND documents.insurance_id = 0 ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id1, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> necessaryDocumentsByBuyMethod(String id, int buyMethod) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM documents " +
					"JOIN customer " +
					"ON documents.status_id = customer.status_id " +
					"WHERE customer.id = ? " +
					"AND documents.buy_method_id = ? " ;

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.setInt(2, buyMethod);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id1, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> necessaryDocumentsByTradeInCar(String id, int tradeInCar) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM documents " +
					"JOIN customer " +
					"ON documents.status_id = customer.status_id " +
					"WHERE customer.id = ? " +
					"AND documents.trade_in_car_id = ? " ;

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.setInt(2, tradeInCar);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id1, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> necessaryDocumentsByInsurance(String id, int insurance) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM documents " +
					"JOIN customer " +
					"ON documents.status_id = customer.status_id " +
					"WHERE customer.id = ? " +
					"AND documents.insurance_id = ? " ;

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.setInt(2, insurance);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id1, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> necessaryDocuments(String id,int buyMethodId, int tradeInCarId, int insuranceId) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM documents " +
					"JOIN customer " +
					"ON documents.status_id = customer.status_id " +
					"WHERE customer.id = ? "+
					"AND documents.buy_method_id = ? "+
					"AND documents.trade_in_car_id = ? " +
					"AND documents.insurance_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.setInt(2, buyMethodId);
			pStmt.setInt(3, tradeInCarId);
			pStmt.setInt(4, insuranceId);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id1, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> necessaryDocuments1(int buyMethodId) {
		Connection conn = null;
		List<Documents> documentsList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * " +
					"FROM documents " +
					"JOIN customer " +
					"ON documents.status_id = customer.status_id " +
					"WHERE customer.id = 1 "+
					"AND documents.buy_method_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, buyMethodId);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents documents = new Documents(id1, name, file, statusId);

				documentsList.add(documents);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return documentsList;
	}

	public List<Documents> getSubmissionStatus(String customerId) {
		Connection conn = null;
		List<Documents> dmList = new ArrayList<Documents>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * "
					+ "FROM documents "
					+ "INNER JOIN documents_management "
					+ "ON documents.id = documents_management.documents_id "
					+ "INNER JOIN customer "
					+ "ON customer.id = documents_management.customer_id "
					+ "WHERE customer.id = ? "
					+ "AND documents.status_id = customer.status_id";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, customerId);

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("documents_name");
				String file = rs.getString("file");
				int statusId = rs.getInt("status_id");
				Documents dm = new Documents(id1, name, file, statusId);

				dmList.add(dm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return dmList;
	}
}
