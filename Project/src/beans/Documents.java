package beans;

public class Documents {

	private int id;
	private String name;
	private String file;
	private int statusId;

	public Documents(int id, String name, String file, int statusId) {
		this.id = id;
		this.name = name;
		this.file = file;
		this.statusId = statusId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

}
