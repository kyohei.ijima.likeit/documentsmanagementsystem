package beans;

import dao.DocumentsDao;

public class DocumentsManagement {

	private int id;
	private int documentsId;
	private int submissionStatus;
	private int customerId;

	public DocumentsManagement (int id, int documentsId, int submissionStatus, int customerId) {
		this.id = id;
		this.documentsId = documentsId;
		this.submissionStatus = submissionStatus;
		this.customerId = customerId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDocumentsId() {
		return documentsId;
	}

	public void setDocumentsId(int documentsId) {
		this.documentsId = documentsId;
	}

	public int getSubmissionStatus() {
		return submissionStatus;
	}

	public void setSubmissionStatus(int submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	// 以下結合テーブル情報
	public Documents getDocuments() {
		DocumentsDao dao = new DocumentsDao();
		return dao.findById(Integer.toString(this.documentsId));
	}

}
