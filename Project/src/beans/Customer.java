package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import dao.CarDao;
import dao.StaffDao;
import dao.StatusDao;

public class Customer implements Serializable {

	private int id;
	private int receivedOrderId;
	private String name;
	private String address;
	private int age;
	private Date birthDate;
	private int carId;
	private int buyMethod;
	private int tradeInCar;
	private int insurance;
	private int staffId;
	private int statusId;

	// 全てのデータをセットするコンストラクタ
	public Customer(int id, int receivedOrderId, String name, String address, int age, Date birthDate, int carId,
			int buyMethod, int tradeInCar, int insurance, int staffId, int statusId) {
		this.id = id;
		this.receivedOrderId = receivedOrderId;
		this.name = name;
		this.address = address;
		this.age = age;
		this.birthDate = birthDate;
		this.carId = carId;
		this.buyMethod = buyMethod;
		this.tradeInCar = tradeInCar;
		this.insurance = insurance;
		this.staffId = staffId;
		this.statusId = statusId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getReceivedOrderId() {
		return receivedOrderId;
	}

	public void setReceivedOrderId(int receivedOrderId) {
		this.receivedOrderId = receivedOrderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public int getBuyMethod() {
		return buyMethod;
	}

	public void setBuyMethod(int buyMethod) {
		this.buyMethod = buyMethod;
	}

	public int getTradeInCar() {
		return tradeInCar;
	}

	public void setTradeInCar(int tradeInCar) {
		this.tradeInCar = tradeInCar;
	}

	public int getInsurance() {
		return insurance;
	}

	public void setInsurance(int insurance) {
		this.insurance = insurance;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	//誕生日フォーマット
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(birthDate);
	}

	// 以下結合テーブル情報
	public Staff getStaff() {
		StaffDao dao = new StaffDao();
		return dao.findById(this.staffId);
	}

	public Status getStatus() {
		StatusDao dao = new StatusDao();
		return dao.findById(this.statusId);
	}

	public Car getCar() {
		CarDao dao = new CarDao();
		return dao.findById(this.carId);
	}

	public Status getNextStatus() {
		StatusDao dao = new StatusDao();
		return dao.nextStatus(this.statusId);
	}

}
