<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>必要書類一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>

	<div class="mt-5">
		<h1 class="col-3 mx-auto">必要書類一覧</h1>
	</div>

	<div class="mx-auto col-10 mt-4">
		<h2 class="col-5">注文時必要書類</h2>
	</div>

	<div class="col-9 mx-auto mt-2">
		<div class="row">
			<c:forEach var="document1" items="${documentsList1}">
				<c:choose>
					<c:when test="${document1.statusId == 1}">
						<div class="alert alert-primary mt-5 col-5 mr-5" role="alert">
							<a href="DocumentDetail?id=${document1.id}">${document1.name}</a>
						</div>
					</c:when>
				</c:choose>
			</c:forEach>
		</div>
	</div>


	<div class="mx-auto col-10 mt-4">
		<h2 class="col-5">登録時必要書類</h2>
	</div>

	<div class="col-9 mx-auto mt-2">
		<div class="row">
			<c:forEach var="document2" items="${documentsList2}">
				<c:choose>
					<c:when test="${document2.statusId == 2}">
						<div class="alert alert-primary mt-5 col-5 mr-5" role="alert">
							<a href="DocumentDetail?id=${document2.id}">${document2.name}</a>
						</div>
					</c:when>
				</c:choose>
			</c:forEach>
		</div>
	</div>

	<div class="mx-auto col-10 mt-4">
		<h2 class="col-5">納車時必要書類</h2>
	</div>

	<div class="col-9 mx-auto mt-2">
		<div class="row">
			<c:forEach var="document3" items="${documentsList3}">
				<c:choose>
					<c:when test="${document3.statusId == 3}">
						<div class="alert alert-primary mt-5 col-5 mr-5" role="alert">
							<a href="DocumentDetail?id=${document3.id}">${document3.name}</a>
						</div>
					</c:when>
				</c:choose>
			</c:forEach>
		</div>
	</div>


	<div class="mx-auto col-10 mt-4">
		<h2 class="col-5">納車後必要書類</h2>
	</div>

	<div class="col-9 mx-auto mt-2">
		<div class="row">
			<c:forEach var="document4" items="${documentsList4}">
				<c:choose>
					<c:when test="${document4.statusId == 4}">
						<div class="alert alert-primary mt-5 col-5 mr-5" role="alert">
							<a href="DocumentDetail?id=${document4.id}">${document4.name}</a>
						</div>
					</c:when>
				</c:choose>
			</c:forEach>
		</div>
	</div>

	<div class="col-6 mx-auto mt-5">
		<a href="Top" class="btn btn-link float-right mt-5">戻る</a>
	</div>

</body>
</html>