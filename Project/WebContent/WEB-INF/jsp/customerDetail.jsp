<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>
	<div class="row col-5 mx-auto">
		<h1 class="col-5 mx-auto mt-5">顧客詳細</h1>
	</div>

	<div class="col-10 mx-auto">
		<form>
			<div class="form-group">
				<label for="exampleInputEmail1">名前</label> <a class="form-control">${customer.name}</a>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">住所</label> <a
					class="form-control">${customer.address}</a>
			</div>
			<div class="row">
				<div class="form-group col-2">
					<label for="exampleInputPassword1">年齢</label> <a
						class="form-control">${customer.age}</a>
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleInputPassword1">生年月日</label> <a
						class="form-control">${customer.formatDate}</a>
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleInputPassword1">ID</label> <a
						class="form-control">${customer.id}</a>
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleInputPassword1">受注番号</label> <a
						class="form-control">${customer.receivedOrderId}</a>
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleFormControlSelect1">車種</label> <a
						class="form-control">${customer.car.name}</a>
				</div>
			</div>
			<div class="row ">
				<div class="form-group col-2">
					<label for="exampleInputPassword1">購入方法 ：</label>
					<c:choose>
						<c:when test="${customer.buyMethod == 1}">
							<a>現金</a>
						</c:when>
						<c:when test="${customer.buyMethod == 2}">
							<a>割賦</a>
						</c:when>
					</c:choose>
				</div>
				<div class="form-group col-2">
					<label for="exampleInputPassword1">下取り車有無 ：</label>
					<c:choose>
						<c:when test="${customer.tradeInCar == 1}">
							<a>有</a>
						</c:when>
						<c:when test="${customer.tradeInCar == 2}">
							<a>無</a>
						</c:when>
					</c:choose>
				</div>
				<div class="form-group col-2">
					<label for="exampleInputPassword1">保険 ：</label>
					<c:choose>
						<c:when test="${customer.insurance == 1}">
							<a>自社</a>
						</c:when>
						<c:when test="${customer.insurance == 2}">
							<a>他社</a>
						</c:when>
					</c:choose>
				</div>
			</div>
		</form>
	</div>

	<div class="row mt-3 col-10 mx-auto">
		<h2 class="mr-3">Status : ${customer.status.name}</h2>
		<div class="arrow1 mt-2 mr-3"></div>
		<h2 class="ml-3">Next : ${customer.nextStatus.name}</h2>
	</div>
	<div class="progress col-10 mx-auto mt-2">
		<c:choose>
			<c:when test="${customer.statusId == 1}">
				<div class="progress-bar" role="progressbar" style="width: 20%;"
					aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
			</c:when>
			<c:when test="${customer.statusId == 2}">
				<div class="progress-bar" role="progressbar" style="width: 40%;"
					aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">40%</div>
			</c:when>
			<c:when test="${customer.statusId == 3}">
				<div class="progress-bar" role="progressbar" style="width: 60%;"
					aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
			</c:when>
			<c:when test="${customer.statusId == 4}">
				<div class="progress-bar" role="progressbar" style="width: 80%;"
					aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">80%</div>
			</c:when>
			<c:when test="${customer.statusId == 5}">
				<div class="progress-bar" role="progressbar" style="width: 100%;"
					aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
			</c:when>
		</c:choose>
	</div>

	<c:choose>
		<c:when test="${customer.statusId != 5}">
			<div class="mx-auto col-10 mt-4">
				<h3 class="col-5">${customer.name}様
					${customer.nextStatus.name}時必要書類</h3>
			</div>

			<form action="CustomerDetail" method="post">
				<div class="col-9 mx-auto mt-2">
					<div class="row">
						<c:forEach var="dm" items="${dmList}">
							<div class="alert alert-primary mt-5 col-5 mr-5" role="alert">
								<c:choose>
									<c:when test="${dm.submissionStatus == 1}">
									</c:when>
									<c:otherwise>
										<input type="checkbox" class="form-check-input"
											id="exampleCheck1" value="${dm.documentsId}"
											name="documentId">
									</c:otherwise>
								</c:choose>
								<a href="DocumentDetail?id=${dm.documentsId}">${dm.documents.name}</a>
							</div>
						</c:forEach>
					</div>
				</div>
				<div class="col-7 mx-auto mt-5">
					<button type="submit"
						class="btn btn-primary mt-2 float-right col-2"
						value="${customer.id}" name="customerId">更新</button>
				</div>
			</form>
		</c:when>
		<c:otherwise>
			<div class="mx-auto col-10 mt-4">
				<h3 class="col-5">お手続きお疲れ様でした！</h3>
			</div>
		</c:otherwise>
	</c:choose>

</body>
</html>