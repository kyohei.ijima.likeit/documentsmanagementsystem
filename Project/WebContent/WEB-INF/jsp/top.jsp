<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TOPページ</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>
	<div class="row col-5 mx-auto mt-5">
		<h1 class="col-6 mx-auto mt-5">書類管理システム</h1>
	</div>
	<div class="row col-10 mx-auto mt-5">
		<div class="card ml-5 mr-5" style="width: 18rem;">
			<img src="img/友達検索アイコン.png" height="270"
				class="card-img-top" alt="検索の画像">
			<div class="card-body">
				<h5 class="card-title">顧客検索</h5>
				<p class="card-text">${staffInfo.name}さんが今手続き中の顧客を一覧で表示します。顧客名からその顧客の詳細、ステータス、書類状況を確認できます</p>
				<a href="CustomerSearch" class="btn btn-primary">顧客検索画面へ</a>
			</div>
		</div>
		<div class="card ml-5 mr-5" style="width: 18rem;">
			<img src="img/人物シルエット　プラス.png" height="270"
				class="card-img-top" alt="鉛筆の画像">
			<div class="card-body">
				<h5 class="card-title">顧客追加</h5>
				<p class="card-text">新しく顧客を追加します。また、追加した顧客の受注時に必要な書類をご案内します。</p>
				<a href="AddCustomer" class="btn btn-primary">顧客追加画面へ</a>
			</div>
		</div>
		<div class="card ml-5 mr-5" style="width: 18rem;">
			<img src="img/友だちの無料アイコン3.png" height="270"
				class="card-img-top" alt="顧客の画像">
			<div class="card-body">
				<h5 class="card-title">販売履歴</h5>
				<p class="card-text">${staffInfo.name}さんが今まで販売した顧客を確認できます。</p>
				<a href="FinishedCustomerSearch" class="btn btn-primary">販売履歴画面へ</a>
			</div>
		</div>
		<div class="card ml-5 mr-5" style="width: 18rem;">
			<img src="img/書類アイコン.png" height="270"
				class="card-img-top" alt="書類の画像">
			<div class="card-body">
				<h5 class="card-title">書類検索</h5>
				<p class="card-text">受注から納車までに必要な書類の一覧を表示します。詳細画面にて書類の書き方や注意点の確認ができます。</p>
				<a href="DocumentsList" class="btn btn-primary">書類一覧へ</a>
			</div>
		</div>
	</div>

	<c:choose>
	<c:when test="${staffInfo.loginId == 'a'}">
	<div class="list-group mt-5 col-8 mx-auto">
		<a class="list-group-item">管理者メニュー</a> <a href="StaffList"
			class="list-group-item list-group-item-action list-group-item-primary">スタッフ情報管理</a>
		<a href="AllCustomerSearch"
			class="list-group-item list-group-item-action list-group-item-secondary">顧客情報管理</a>
		<a href="CarList"
			class="list-group-item list-group-item-action list-group-item-danger">車種管理</a>
	</div>
	</c:when>
	</c:choose>

</body>
</html>