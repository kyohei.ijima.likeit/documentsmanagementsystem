<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<style type="text/css">
h1 {
	font-size: 50px;
	position: relative;
	margin-top: 80px;
	text-align: center;
}

a.customerUpdate{
	font-size: x-large;
}
</style>

</head>
<body>
	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>
	<h1>顧客情報更新</h1>

	<div class="mt-3 col-9 mx-auto">
		<a class="customerUpdate">更新したい顧客情報を新しく入力してください</a>
	</div>

	<div class="col-10 mx-auto mt-3">
		<form action='CustomerUpdate' method='post'>
			<div class="form-group">
				<label for="exampleInputEmail1">名前</label> <input name="name"
					class="form-control" id="exampleInputEmail1"
					aria-describedby="emailHelp" value="${customer.name}">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">住所</label> <input name="address"
					class="form-control" id="exampleInputPassword1"
					value="${customer.address}">
			</div>
			<div class="row">
				<div class="form-group col-2">
					<label for="exampleInputPassword1">年齢</label> <input name="age"
						class="form-control" id="exampleInputPassword1"
						value="${customer.age}">
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleInputPassword1">生年月日</label> <input
						name="birthDate" class="form-control" id="exampleInputPassword1"
						value="${customer.birthDate}">
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleInputPassword1">ID ※変更できません</label> <a
						class="form-control">${customer.id}</a> <input type="hidden"
						value="${customer.id}" name="id">
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleInputPassword1">受注番号</label> <input
						name="receivedOrderId" class="form-control"
						id="exampleInputPassword1" value="${customer.receivedOrderId}">
				</div>
				<div class="form-group col-2 ml-5">
					<label for="exampleFormControlSelect1">車種 ※変更できません</label> <a
						class="form-control">${customer.car.name}</a>
				</div>
			</div>
			<div class="row ">
				<div class="form-group col-2">
					<label for="exampleInputPassword1">購入方法 ：</label>
					<c:choose>
						<c:when test="${customer.buyMethod == 1}">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="buyMethod"
									id="inlineRadio1" value="1" checked="checked"> <label
									class="form-check-label" for="inlineRadio1">現金</label>
							</div>
							<div class="form-check form-check-inline mt-2">
								<input class="form-check-input" type="radio" name="buyMethod"
									id="inlineRadio2" value="2"> <label
									class="form-check-label" for="inlineRadio2">ローン</label>
							</div>
						</c:when>
						<c:when test="${customer.buyMethod == 2}">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="buyMethod"
									id="inlineRadio1" value="1"> <label
									class="form-check-label" for="inlineRadio1">現金</label>
							</div>
							<div class="form-check form-check-inline mt-2">
								<input class="form-check-input" type="radio" name="buyMethod"
									id="inlineRadio2" value="2" checked="checked"> <label
									class="form-check-label" for="inlineRadio2">ローン</label>
							</div>
						</c:when>
					</c:choose>
				</div>
				<div class="form-group col-2">
					<label for="exampleInputPassword1">下取り車有無 ：</label>
					<c:choose>
						<c:when test="${customer.tradeInCar == 1}">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="tradeInCar"
									id="inlineRadio1" value="1" checked="checked"> <label
									class="form-check-label" for="inlineRadio1">有</label>
							</div>
							<div class="form-check form-check-inline mt-2">
								<input class="form-check-input" type="radio" name="tradeInCar"
									id="inlineRadio2" value="2"> <label
									class="form-check-label" for="inlineRadio2">無</label>
							</div>
						</c:when>
						<c:when test="${customer.tradeInCar == 2}">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="tradeInCar"
									id="inlineRadio1" value="1"> <label
									class="form-check-label" for="inlineRadio1">有</label>
							</div>
							<div class="form-check form-check-inline mt-2">
								<input class="form-check-input" type="radio" name="tradeInCar"
									id="inlineRadio2" value="2" checked="checked"> <label
									class="form-check-label" for="inlineRadio2">無</label>
							</div>
						</c:when>
					</c:choose>
				</div>
				<div class="form-group col-2">
					<label for="exampleInputPassword1">保険 ：</label>
					<c:choose>
						<c:when test="${customer.insurance == 1}">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="insurance"
									id="inlineRadio1" value="1" checked="checked"> <label
									class="form-check-label" for="inlineRadio1">自社</label>
							</div>
							<div class="form-check form-check-inline mt-2">
								<input class="form-check-input" type="radio" name="insurance"
									id="inlineRadio2" value="2"> <label
									class="form-check-label" for="inlineRadio2">他社</label>
							</div>
						</c:when>
						<c:when test="${customer.insurance == 2}">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="insurance"
									id="inlineRadio1" value="1"> <label
									class="form-check-label" for="inlineRadio1">自社</label>
							</div>
							<div class="form-check form-check-inline mt-2">
								<input class="form-check-input" type="radio" name="insurance"
									id="inlineRadio2" value="2" checked="checked"> <label
									class="form-check-label" for="inlineRadio2">他社</label>
							</div>
						</c:when>
					</c:choose>
				</div>
			</div>
			<div>
				<button type="submit" class="btn btn-primary mt-2">更新</button>
			</div>
		</form>
	</div>

	<div class="col-10 mx-auto mt-5">
		<a href="AllCustomerSearch" class="btn btn-link float-left mt-5">戻る</a>
	</div>

</body>
</html>