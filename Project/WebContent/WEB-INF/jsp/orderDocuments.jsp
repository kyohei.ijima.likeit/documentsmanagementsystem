<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${newCustomer.name}様 受注時必要書類</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
            <ul class="navbar navbar-dark bg-dark justify-content-end">
                <div class="col-10">
            <a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
        </div>
           <li class="nav-item">
            <a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
          </li>
         <li class="nav-item">
            <a class="nav-link navbar-brand" href="Logout">ログアウト</a>
          </li>
        </ul>

    <div class="mx-auto mt-5 col-8">
     <h1 class="col-5 mx-auto mt-5">${newCustomer.name}様　受注時必要書類</h1>
    </div>

    <div class="col-5 mx-auto mt-2">
    <c:forEach var="document" items="${documentsList}">
    <div class="alert alert-primary mt-5" role="alert">
        <a href="DocumentDetail?id=${document.id}">${document.name}</a>
    </div>
   </c:forEach>
    </div>

    <div class="mt-5 col-5 mx-auto">
    <a href="Top" type="button" class="btn btn-link float-right mt-5">Topへ</a>
    </div>

</body>
</html>