<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>車種一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>
	<div class="col-10 mt-5">
		<a href="AddNewCar" class="float-right">新規追加</a>
	</div>

	<div class="row col-5 mx-auto mt-5">
		<h1 class="col-5 mx-auto">車種一覧</h1>
	</div>

	<form>
		<div class="form-group row col-5 mx-auto mt-3">
			<label for="inputPassword" class="col-sm-2 col-form-label">型式</label>
			<div class="col-sm-10">
				<input type="loginId" class="form-control" id="inputloginId">
			</div>
		</div>
		<div class="form-group row row col-5 mx-auto mt-3">
			<label for="inputPassword" class="col-sm-2 col-form-label">車名</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" id="inputPassword">
			</div>
		</div>

	</form>
	<div class="col-6 mx-auto">
		<button type="button" class="btn-clipboard float-right" title=""
			data-original-title="Copy to clipboard mt-50">検索</button>
	</div>

	<div class="col-8 mx-auto">
		<p class="box"></p>
	</div>


	<table border class="col-6 mx-auto mt-5">
		<tr>
			<th>型式</th>
			<th>車名</th>
			<th></th>
		</tr>
		<c:forEach var="car" items="${carList}">
		<tr>
			<td>${car.type}</td>
			<td>${car.name}</td>
			<td>
				<div class="btn-group btn-group-sm btn btn-danger" role="group"
					aria-label="...">
					<a href="CarDelete?id=${car.id}">削除</a>
				</div>
			</td>
		</tr>
		</c:forEach>
	</table>


</body>
</html>