<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>顧客検索</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <style type="text/css">
  	h1{
        font-size: 50px;
        position:relative;
        margin-top: 80px;
        text-align:center;
  	}

	</style>

</head>
<body>
     <ul class="navbar navbar-dark bg-dark justify-content-end">
         <div class="col-10">
            <a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
        </div>
           <li class="nav-item">
            <a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
          </li>
         <li class="nav-item">
            <a class="nav-link navbar-brand" href="Logout">ログアウト</a>
          </li>
        </ul>
    <h1>顧客検索</h1>

    <div class="col-10 mx-auto">
        <form action='CustomerSearch' method='post'>
    <div class="form-group">
        <label for="exampleInputEmail1">名前</label>
        <input name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">住所</label>
                <input name="address" class="form-control" id="exampleInputPassword1" placeholder="Addless">
            </div>
            <div class="row">
            <div class="form-group col-2">
                <label for="exampleInputPassword1">年齢</label>
                <input name="age" class="form-control" id="exampleInputPassword1" placeholder="Agg">
            </div>
            <div class="form-group col-2 ml-5">
                <label for="exampleInputPassword1">生年月日</label>
                <input name="birthDate" class="form-control" id="exampleInputPassword1" placeholder="BirthDay">
            </div>
            <div class="form-group col-2 ml-5">
                <label for="exampleInputPassword1">ID</label>
                <input name="id" class="form-control" id="exampleInputPassword1" placeholder="Number">
            </div>
            <div class="form-group col-2 ml-5">
                <label for="exampleInputPassword1">受注番号</label>
                <input name="receivedOrderId" class="form-control" id="exampleInputPassword1" placeholder="ID">
            </div>
            <div class="form-group col-2 ml-5">
                <label for="exampleFormControlSelect1">車種</label>
                <select class="form-control" name="car" id="exampleFormControlSelect1">
                	<option value="">選択してください</option>
                <c:forEach var="car" items="${carList}">
                    <option value="${car.id}">${car.name}</option>
                </c:forEach>
                </select>
            </div>
            </div>
            <div class="row ">
                <div class="form-group col-2">
                    <label for="exampleInputPassword1">購入方法　：</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="buyMethod" id="inlineRadio1" value="1">
                        <label class="form-check-label" for="inlineRadio1">現金</label>
                    </div>
                    <div class="form-check form-check-inline mt-2">
                        <input class="form-check-input" type="radio" name="buyMethod" id="inlineRadio2" value="2">
                        <label class="form-check-label" for="inlineRadio2">ローン</label>
                    </div>
                </div>
                <div class="form-group col-2">
                    <label for="exampleInputPassword1">下取り車有無　：</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="tradeInCar" id="inlineRadio1" value="1">
                        <label class="form-check-label" for="inlineRadio1">有</label>
                    </div>
                    <div class="form-check form-check-inline mt-2">
                        <input class="form-check-input" type="radio" name="tradeInCar" id="inlineRadio2" value="2">
                        <label class="form-check-label" for="inlineRadio2">無</label>
                    </div>
                </div>
                <div class="form-group col-2">
                    <label for="exampleInputPassword1">保険　：</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="insurance" id="inlineRadio1" value="1">
                        <label class="form-check-label" for="inlineRadio1">自社</label>
                    </div>
                    <div class="form-check form-check-inline mt-2">
                        <input class="form-check-input" type="radio" name="insurance" id="inlineRadio2" value="2">
                        <label class="form-check-label" for="inlineRadio2">他社</label>
                    </div>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-primary mt-2">検索</button>
            </div>
        </form>
    </div>

        <table border="2" class="col-7 mt-5 mx-auto">
        <c:forEach var="customer" items="${customerList}">
            <tr>
                <th>ID : ${customer.id}</th>
                <th>受注番号 : ${customer.receivedOrderId}</th>
                <th>Name : <a href="CustomerDetail?customerId=${customer.id}">${customer.name}</a></th>
                <td>年齢 : ${customer.age}</td>
                <td>車種 : ${customer.car.name}</td>
                <td>Status : ${customer.status.name}</td>
                <td>担当 : ${customer.staff.name}</td>
            </tr>
           </c:forEach>
        </table>

</body>
</html>