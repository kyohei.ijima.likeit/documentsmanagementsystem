<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>スタッフ削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<style>
h1 {
	font-size: 50px;
	position: relative;
	margin-top: 50px;
	text-align: center;
}
</style>
</head>
<body>

	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>

	<h1>スタッフ削除確認</h1>
	<div class="mt-5 col-5 mx-auto">
		<p>ログインID：${staffDetail.loginId}</p>
		<p>を本当に削除してよろしいでしょうか。</p>
	</div>

	<div class="row col-6 mx-auto mt-5">
		<div class=" mx-auto">
			<input type="button" value="キャンセル"
				onClick="location.href='StaffList'"
				class="col-12 mt-5 mx-aoto">
		</div>
		<div class="col-2 mx-auto">
			<form action="StaffDelete" method="post">
				<input type="submit" value="OK"
					class="btn-clipboard col-12 mt-5 mx-aoto"> <input
					type="hidden" value="${staffDetail.id}" name="id">
			</form>
		</div>
	</div>
</body>
</html>