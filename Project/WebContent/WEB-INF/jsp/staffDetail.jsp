<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>スタッフ情報詳細参照</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <style>
    h1{
        font-size: 50px;
        position:relative;
        margin-top: 50px;
        text-align:center;
    }

    </style>
</head>
	<body>

	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<div class="col-10">
			<a class="nav-link navbar-brand float-left" href="Top">書類管理システム</a>
		</div>
		<li class="nav-item"><a class="nav-link navbar-brand" href="#">${staffInfo.name}さん</a>
		</li>
		<li class="nav-item"><a class="nav-link navbar-brand"
			href="Logout">ログアウト</a></li>
	</ul>
    <h1>スタッフ情報詳細参照</h1>

  <div class="form-group row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <label for="inputPassword" class="col-sm-8 float-right col-form-label">${staffDetail.loginId}</label>
    </div>
  </div>
  <div class="form-group row row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">ユーザ名</label>
    <div class="col-sm-10">
      <label for="inputPassword" class="col-sm-8 float-right col-form-label">${staffDetail.name}</label>
    </div>
  </div>
  <div class="form-group row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">生年月日</label>
  <div class="col-sm-10">
      <label for="inputPassword" class="col-sm-8 float-right col-form-label">${staffDetail.birthDate}</label>
    </div>
  </div>
<div class="form-group row row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">登録日時</label>
    <div class="col-sm-10">
      <label for="inputPassword" class="col-sm-8 float-right col-form-label">${staffDetail.createDate}</label>
    </div>
  </div>
 <div class="form-group row row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">更新日</label>
    <div class="col-sm-10">
      <label for="inputPassword" class="col-sm-8 float-right col-form-label">${staffDetail.updateDate}</label>
    </div>
  </div>

 <div class="col-6 mx-auto mt-5">
   <a href="StaffList" class="btn btn-link float-left mt-5">戻る</a>
    </div>

	</body>
</html>