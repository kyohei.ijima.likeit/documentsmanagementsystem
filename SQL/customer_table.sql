CREATE TABLE customer (
	id SERIAL PRIMARY KEY UNIQUE NOT NULL,
  received_order_id INT NOT NULL,
	name VARCHAR(255) NOT NULL,
  address VARCHAR(255) NOT NULL,
  age INT NOT NULL,
	birth_date DATE NOT NULL,
	car_id INT NOT NULL,
  buy_method INT NOT NULL,
  trade_in_car INT NOT NULL,
  insurance INT NOT NULL,
  staff_id INT NOT NULL,
  status_id INT NOT NULL,
	);
