CREATE TABLE documents_management (
	id SERIAL PRIMARY KEY UNIQUE NOT NULL,
	documents_id INT NOT NULL,
  submission_status INT NOT NULL,
  customer_id INT NOT NULL
	);
